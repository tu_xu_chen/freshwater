package org.wdsystem.file.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wdsystem.file.service.FileService;
import org.wdsystem.file.service.FileUploadService;
import org.wdsystem.file.service.internal.FileUploadServiceImpl;
import org.wdsystem.file.service.internal.SimpleFileServiceImpl;

/**
 * 文件服务的配置信息
 * @author tuxuchen
 * @date 2022/8/3 14:28
 */
@Configuration
public class FileConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public FileService getFileService() {
    return new SimpleFileServiceImpl();
  }

  @Bean
  @ConditionalOnMissingBean
  public FileUploadService getFileUploadService() {
    return new FileUploadServiceImpl();
  }

}
