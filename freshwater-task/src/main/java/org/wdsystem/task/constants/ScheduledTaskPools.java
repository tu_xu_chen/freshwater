package org.wdsystem.task.constants;

import org.springframework.scheduling.config.CronTask;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

/**
 * 定时任务池
 * @author tuxuchen
 * @date 2022/9/26 16:44
 */
public class ScheduledTaskPools {

  private static volatile ScheduledTaskPools instance;

  private ConcurrentHashMap<String, ScheduledFuture<?>> scheduledFutures = new ConcurrentHashMap<>();

  private ConcurrentHashMap<String, CronTask> cronTasks = new ConcurrentHashMap<>();

  private ScheduledTaskPools() {}

  /**
   * 添加任务
   * @param key
   * @param future
   * @param cronTask
   */
  public void add(String key, ScheduledFuture<?> future, CronTask cronTask) {
    scheduledFutures.put(key, future);
    cronTasks.put(key, cronTask);
  }

  /**
   * 删除任务
   * @param key
   */
  public void remove(String key) {
    if (scheduledFutures.containsKey(key)) {
      scheduledFutures.get(key).cancel(true);
      scheduledFutures.remove(key);
      cronTasks.remove(key);
    }
  }

  public static ScheduledTaskPools getInstance() {
    if (instance == null) {
      synchronized (ScheduledTaskPools.class) {
        if (instance == null) {
          instance = new ScheduledTaskPools();
        }
      }
    }
    return instance;
  }

}
