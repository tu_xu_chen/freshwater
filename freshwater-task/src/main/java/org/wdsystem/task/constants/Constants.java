package org.wdsystem.task.constants;

/**
 * 静态变量
 * @author tuxuchen
 * @date 2022/9/26 16:51
 */
public class Constants {

  private Constants() {
    throw new UnsupportedOperationException();
  }

  /**
   * 动态数据源编码前缀
   */
  public static final String TASK_PREFIX = "task_";

}
