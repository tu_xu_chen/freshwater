package org.wdsystem.task.constants;

/**
 * reids键定义
 * @author tuxuchen
 * @date 2022/9/26 16:49
 */
public class RedisKeys {

  private RedisKeys() {
    throw new UnsupportedOperationException();
  }

  /**
   * 动态任务编码生成索引
   */
  public static final String DYNAMIC_TASK_CODE_INDEX = "dynamic:task:code:index";

}
