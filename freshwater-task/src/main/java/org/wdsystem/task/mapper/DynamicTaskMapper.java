package org.wdsystem.task.mapper;

import org.apache.ibatis.annotations.Param;
import org.wdsystem.task.dto.DynamicTaskQueryDTO;
import org.wdsystem.task.entity.DynamicTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 动态定时任务 Mapper 接口
 * </p>
 *
 * @author tuxuchen
 * @since 2022-09-26 16:35
 */
public interface DynamicTaskMapper extends BaseMapper<DynamicTaskEntity> {

  /**
   * 多条件查询
   * @param query
   * @return
   */
  List<DynamicTaskEntity> selectByConditions(@Param("query") DynamicTaskQueryDTO query);

  /**
   * 根据定时任务编码查询
   * @param code
   * @return
   */
  DynamicTaskEntity selectByCode(@Param("code") String code);

  /**
   * 根据定时任务key查询
   * @param taskKey
   * @return
   */
  DynamicTaskEntity selectByTaskKey(@Param("taskKey")  String taskKey);
}
