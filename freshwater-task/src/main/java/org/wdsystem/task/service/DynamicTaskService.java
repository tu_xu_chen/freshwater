package org.wdsystem.task.service;

import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Pageable;
import org.wdsystem.task.dto.DynamicTaskQueryDTO;
import org.wdsystem.task.entity.DynamicTaskEntity;

import java.util.List;

/**
 * <p>
 * 动态定时任务 服务类
 * </p>
 *
 * @author tuxuchen
 * @since 2022-09-26 16:35
 */
public interface DynamicTaskService {

  /**
   * 创建动态定时任务
   * @param dynamicTask
   * @return
   */
  DynamicTaskEntity create(DynamicTaskEntity dynamicTask);

  /**
   * 更新动态定时任务
   * @param dynamicTask
   * @return
   */
  DynamicTaskEntity update(DynamicTaskEntity dynamicTask);

  /**
   * 根据主键ID删除
   * @param id
   */
  void deleteById(String id);

  /**
   * 根据ID查询动态定时任务
   * @param id
   * @return
  */
  DynamicTaskEntity findById(String id);

  /**
   * 根据状态查询定时任务
   * @param state 状态
   * @return
   */
  List<DynamicTaskEntity> findByState(Integer state);

  /**
   * 根据定时任务Key查询
   * @param taskKey
   * @return
   */
  DynamicTaskEntity findByTaskKey(String taskKey);

  /**
   * 多条件查询
   * @param query
   * @param pageable
   * @return
   */
  PageInfo findByConditions(DynamicTaskQueryDTO query, Pageable pageable);

  /**
   * 立即执行定时任务
   * @param code
   */
  void execute(String code);
}
