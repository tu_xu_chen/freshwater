package org.wdsystem.task.service.listener;

import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.wdsystem.task.entity.DynamicTaskEntity;

/**
 * 定时任务执行器
 * @author tuxuchen
 * @date 2022/9/22 14:34
 */
public interface ScheduledTaskListener {

  /**
   * 执行器初始化
   * @param taskRegistrar
   */
  void init(ScheduledTaskRegistrar taskRegistrar);

  /**
   * 刷新定时任务
   * @param task
   */
  void refresh(DynamicTaskEntity task);

}
