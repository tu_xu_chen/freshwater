package org.wdsystem.task.service.task;

import org.wdsystem.common.constants.enums.NormalStatusEnum;
import org.wdsystem.common.utils.ApplicationContextUtils;
import org.wdsystem.task.entity.DynamicTaskEntity;
import org.wdsystem.task.service.DynamicTaskService;

/**
 * 定时任务接口
 * @author tuxuchen
 * @date 2022/9/21 16:30
 */
public interface ScheduledRunnable extends Runnable {

  /**
   * 实际要处理的任务
   */
  void execute();

  /**
   * 控制定时任务启用或禁用的功能
   */
  @Override
  default void run() {
    DynamicTaskService taskService = ApplicationContextUtils.getBean(DynamicTaskService.class);
    DynamicTaskEntity task = taskService.findByTaskKey(this.getClass().getName());
    if (task == null || NormalStatusEnum.DISABLE.getStatus().equals(task.getState())) {
      return;
    }
    execute();
  }
}
