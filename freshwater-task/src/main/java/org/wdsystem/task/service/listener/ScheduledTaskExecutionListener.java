package org.wdsystem.task.service.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;
import org.wdsystem.common.utils.ApplicationContextUtils;
import org.wdsystem.task.constants.ScheduledTaskPools;
import org.wdsystem.task.entity.DynamicTaskEntity;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;

/**
 * 定时任务执行器实现类
 * @author tuxuchen
 * @date 2022/9/22 14:38
 */
@Slf4j
@Component
public class ScheduledTaskExecutionListener implements ScheduledTaskListener {

  private ScheduledTaskRegistrar taskRegistrar;

  @Override
  public void init(ScheduledTaskRegistrar taskRegistrar) {
    log.info("定时任务执行器初始化");
    this.taskRegistrar = taskRegistrar;
    taskRegistrar.setScheduler(Executors.newScheduledThreadPool(30));
  }

  @Override
  public void refresh(DynamicTaskEntity task) {
    ScheduledTaskPools scheduledTaskPools = ScheduledTaskPools.getInstance();
    scheduledTaskPools.remove(task.getCode());
    Object scheduledTask = null;
    try {
      Class<?> clazz = Class.forName(task.getTaskKey());
      scheduledTask = ApplicationContextUtils.getBean(clazz);
    } catch (Exception e) {
      log.error("定时任务加载失败:{}", task.getName());
      log.error(e.getMessage());
    }
    if (scheduledTask != null) {
      // 根据给定的Trigger添加要触发的 Runnable 任务。
      log.info("加载定时任务:{}", task.getTaskKey());
      CronTask cronTask = new CronTask((Runnable) scheduledTask, task.getSrcn());
      ScheduledFuture<?> future = taskRegistrar.getScheduler().schedule(cronTask.getRunnable(), cronTask.getTrigger());
      scheduledTaskPools.add(task.getCode(), future, cronTask);
    }
  }

}
