package org.wdsystem.task.service.internal;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wdsystem.common.constants.enums.NormalStatusEnum;
import org.wdsystem.common.group.CreateValidate;
import org.wdsystem.common.group.UpdateValidate;
import org.wdsystem.common.service.RedisService;
import org.wdsystem.common.utils.ApplicationContextUtils;
import org.wdsystem.common.utils.ValidationUtils;
import org.wdsystem.rbac.entity.UserEntity;
import org.wdsystem.rbac.utils.SecurityUtils;
import org.wdsystem.task.constants.Constants;
import org.wdsystem.task.constants.RedisKeys;
import org.wdsystem.task.dto.DynamicTaskQueryDTO;
import org.wdsystem.task.entity.DynamicTaskEntity;
import org.wdsystem.task.mapper.DynamicTaskMapper;
import org.wdsystem.task.service.DynamicTaskService;
import org.wdsystem.task.service.listener.ScheduledTaskListener;
import org.wdsystem.task.service.task.ScheduledRunnable;

import java.time.LocalDateTime;
import java.util.List;

import static org.wdsystem.common.constants.Constants.DEFAULT_PAGEABLE;

/**
 * <p>
 * 动态定时任务 服务实现类
 * </p>
 *
 * @author tuxuchen
 * @since 2022-09-26 16:35
 */
@Slf4j
@Service
public class DynamicTaskServiceImpl extends ServiceImpl<DynamicTaskMapper, DynamicTaskEntity> implements DynamicTaskService {

  @Autowired
  private RedisService redisService;
  @Autowired
  private ScheduledTaskListener scheduledTaskListener;

  @Override
  @Transactional
  public DynamicTaskEntity create(DynamicTaskEntity dynamicTask) {
    ValidationUtils.validate(dynamicTask, CreateValidate.class);
    String code = redisService.getAndIncrement(RedisKeys.DYNAMIC_TASK_CODE_INDEX, 1, 4);
    code = StringUtils.join(Constants.TASK_PREFIX, code);
    dynamicTask.setCode(code);
    baseMapper.insert(dynamicTask);
    return dynamicTask;
  }

  @Override
  @Transactional
  public DynamicTaskEntity update(DynamicTaskEntity dynamicTask) {
    ValidationUtils.validate(dynamicTask, UpdateValidate.class);
    DynamicTaskEntity dbDynamicTask = baseMapper.selectById(dynamicTask.getId());
    Validate.notNull(dbDynamicTask, "更新的动态定时任务不存在");
    UserEntity userInfo = SecurityUtils.getCurrentUser();
    dbDynamicTask.setSrcn(dynamicTask.getSrcn());
    dbDynamicTask.setState(dynamicTask.getState());
    dbDynamicTask.setModifier(userInfo.getUserAccount());
    dbDynamicTask.setModifierName(userInfo.getRealName());
    dbDynamicTask.setModifyTime(LocalDateTime.now());
    baseMapper.updateById(dbDynamicTask);
    scheduledTaskListener.refresh(dbDynamicTask);
    return dbDynamicTask;
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    Validate.notBlank(id, "主键ID不能为空");
    baseMapper.deleteById(id);
  }

  @Override
  public DynamicTaskEntity findById(String id) {
    if(StringUtils.isBlank(id)) {
      return null;
    }
    return baseMapper.selectById(id);
  }

  @Override
  public List<DynamicTaskEntity> findByState(Integer state) {
    if (state == null) {
      return Lists.newArrayList();
    }
    DynamicTaskQueryDTO query = new DynamicTaskQueryDTO();
    query.setState(state);
    return baseMapper.selectByConditions(query);
  }

  @Override
  public DynamicTaskEntity findByTaskKey(String taskKey) {
    if (StringUtils.isBlank(taskKey)) {
      return null;
    }
    return baseMapper.selectByTaskKey(taskKey);
  }

  @Override
  public PageInfo findByConditions(DynamicTaskQueryDTO query, Pageable pageable) {
    pageable = ObjectUtils.defaultIfNull(pageable, DEFAULT_PAGEABLE);
    PageHelper.startPage(pageable.getPageNumber(), pageable.getPageSize());
    List<DynamicTaskEntity> tasks = baseMapper.selectByConditions(query);
    PageHelper.clearPage();
    return PageInfo.of(tasks);
  }

  @Override
  public void execute(String code) {
    Validate.notBlank(code, "定时任务编码为空!");
    DynamicTaskEntity task = baseMapper.selectByCode(code);
    Validate.notNull(task, "定时任务不存在,编码:%s", code);
    Validate.isTrue(NormalStatusEnum.ENABLE.getStatus().equals(task.getState()), "该定时任务:%s已被禁用", task.getName());
    Object scheduledTask = null;
    try {
      Class<?> clazz = Class.forName(task.getTaskKey());
      scheduledTask = ApplicationContextUtils.getBean(clazz);
    } catch (Exception e) {
      log.error("加载定时任务失败:{}", task.getName());
    }
    ScheduledRunnable runnable = (ScheduledRunnable) scheduledTask;
    runnable.execute();
  }

}
