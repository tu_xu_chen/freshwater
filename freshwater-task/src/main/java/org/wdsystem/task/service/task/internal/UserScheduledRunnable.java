package org.wdsystem.task.service.task.internal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.wdsystem.task.constants.TaskName;
import org.wdsystem.task.service.task.ScheduledRunnable;

/**
 * 用户测试定时任务
 * @author tuxuchen
 * @date 2022/9/21 16:37
 */
@Slf4j
@Component
@TaskName("用户测试定时任务")
public class UserScheduledRunnable implements ScheduledRunnable {

  @Override
  public void execute() {
    log.info("用户测试定时任务");
  }

}
