package org.wdsystem.task.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.wdsystem.common.entity.UuidEntity;
import org.wdsystem.common.group.CreateValidate;
import org.wdsystem.common.group.UpdateValidate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.time.LocalDateTime;

/**
 * <p>
 * 动态定时任务实体
 * </p>
 *
 * @author: tuxuchen
 * @Date: 2022-09-26 16:35
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("freshwater_dynamic_task")
@ApiModel(value = "动态定时任务对象", description = "动态定时任务")
public class DynamicTaskEntity extends UuidEntity {

  private static final long serialVersionUID = -413298668112588438L;

  /**
   * 编码
   */
  @ApiModelProperty(value = "编码")
  @Length(max = 32, message = "编码字符长度不能超过32!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private String code;

  /**
   * 任务名称
   */
  @ApiModelProperty(value = "任务名称")
  @Length(max = 32, message = "任务名称字符长度不能超过32!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  @NotBlank(message = "任务名称不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private String name;

  /**
   * 定时任务class路径
   */
  @ApiModelProperty(value = "定时任务class路径")
  @TableField("task_key")
  @Length(max = 255, message = "定时任务class路径字符长度不能超过255!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  @NotBlank(message = "定时任务class路径不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private String taskKey;

  /**
   * 定时任务表达式
   */
  @ApiModelProperty(value = "定时任务表达式")
  @Length(max = 32, message = "定时任务表达式字符长度不能超过32!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  @NotBlank(message = "定时任务表达式不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private String srcn;

  /**
   * 状态:0关闭1开启
   */
  @ApiModelProperty(value = "状态:0关闭1开启")
  @NotNull(message = "状态:0关闭1开启不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private Integer state;

  /**
   * 创建时间
   */
  @ApiModelProperty(value = "创建时间")
  @TableField("create_time")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @NotNull(message = "创建时间不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private LocalDateTime createTime;

  /**
   * 修改人账号
   */
  @ApiModelProperty(value = "修改人账号")
  @Length(max = 32, message = "修改人账号字符长度不能超过32!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  @NotBlank(message = "修改人账号不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private String modifier;

  /**
   * 修改人姓名
   */
  @ApiModelProperty(value = "修改人姓名")
  @TableField("modifier_name")
  @Length(max = 32, message = "修改人姓名字符长度不能超过32!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  @NotBlank(message = "修改人姓名不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private String modifierName;

  /**
   * 修改时间
   */
  @ApiModelProperty(value = "修改时间")
  @TableField("modify_time")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @NotNull(message = "修改时间不能为空!", groups = {Default.class, CreateValidate.class, UpdateValidate.class})
  private LocalDateTime modifyTime;

  public DynamicTaskEntity() {
    LocalDateTime now = LocalDateTime.now();
    this.createTime = now;
    this.modifier = "ADMIN";
    this.modifierName = "超级管理员";
    this.modifyTime = now;
  }

}
