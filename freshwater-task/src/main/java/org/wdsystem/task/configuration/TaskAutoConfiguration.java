package org.wdsystem.task.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 任务服务自动装配类
 * @author tuxuchen
 * @date 2022/9/21 11:38
 */
@Configuration
@ComponentScan("org.wdsystem.task")
@MapperScan("org.wdsystem.task.mapper")
public class TaskAutoConfiguration {

}
