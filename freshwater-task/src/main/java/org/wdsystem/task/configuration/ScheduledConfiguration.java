package org.wdsystem.task.configuration;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.util.CollectionUtils;
import org.wdsystem.common.constants.enums.NormalStatusEnum;
import org.wdsystem.task.constants.TaskName;
import org.wdsystem.task.entity.DynamicTaskEntity;
import org.wdsystem.task.service.DynamicTaskService;
import org.wdsystem.task.service.listener.ScheduledTaskListener;

import java.util.List;
import java.util.Set;

/**
 * 定时任务配置
 * @author tuxuchen
 * @date 2022/9/21 11:36
 */
@Slf4j
@Configuration
@EnableScheduling
public class ScheduledConfiguration implements SchedulingConfigurer {

  @Autowired
  private DynamicTaskService dynamicTaskService;
  @Autowired
  private ScheduledTaskListener scheduledTaskListener;
  // 定时任务类存放路径
  private static final String TASK_PACKAGE = "org.wdsystem.task.service.task";

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
    List<DynamicTaskEntity> tasks =  this.initTask();
    if (CollectionUtils.isEmpty(tasks)) {
      return;
    }
    scheduledTaskListener.init(taskRegistrar);
    for (DynamicTaskEntity task : tasks) {
      scheduledTaskListener.refresh(task);
    }
  }

  private List<DynamicTaskEntity> initTask() {
    log.info("定时任务初始化");
    Reflections reflections = new Reflections(TASK_PACKAGE);
    Set<Class<?>> classes = reflections.getTypesAnnotatedWith(TaskName.class);
    if (CollectionUtils.isEmpty(classes)) {
      return Lists.newArrayList();
    }
    List<DynamicTaskEntity> tasks = Lists.newArrayList();
    for (Class<?> cla : classes) {
      DynamicTaskEntity task = dynamicTaskService.findByTaskKey(cla.getName());
      if (task == null) {
        task = new DynamicTaskEntity();
        task.setName(cla.getAnnotation(TaskName.class).value());
        task.setTaskKey(cla.getName());
        task.setSrcn("0 */15 * * * ?");
        task.setState(NormalStatusEnum.ENABLE.getStatus());
        dynamicTaskService.create(task);
      }
      tasks.add(task);
    }
    return tasks;
  }

}
