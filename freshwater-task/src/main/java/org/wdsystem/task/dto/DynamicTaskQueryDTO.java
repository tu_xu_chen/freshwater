package org.wdsystem.task.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 定时任务多条件查询
 * @author tuxuchen
 * @date 2022/9/26 17:08
 */
@Data
@ApiModel(value = "定时任务多条件查询DTO", description = "定时任务多条件查询DTO")
public class DynamicTaskQueryDTO {

  @ApiModelProperty(value = "定时任务名称")
  private String name;

  @ApiModelProperty(value = "状态:0关闭1开启")
  private Integer state;

}
