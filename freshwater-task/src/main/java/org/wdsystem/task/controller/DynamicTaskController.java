package org.wdsystem.task.controller;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.wdsystem.common.model.ResponseModel;
import org.wdsystem.common.utils.ResponseModelUtils;
import org.wdsystem.task.dto.DynamicTaskQueryDTO;
import org.wdsystem.task.entity.DynamicTaskEntity;
import org.wdsystem.task.service.DynamicTaskService;

/**
 * <p>
 * 动态定时任务 前端控制器
 * </p>
 *
 * @author tuxuchen
 * @since 2022-09-26
 */
@Api(value = "动态定时任务管理", tags = {"动态定时任务管理"})
@RestController
@RequestMapping("/v1/dynamicTasks")
public class DynamicTaskController {

  @Autowired
  private DynamicTaskService dynamicTaskService;

 /**
  * 更新动态定时任务
  * @param dynamicTask
  * @return
  */
  @PutMapping("")
  @ApiOperation("更新动态定时任务")
  public ResponseModel update(@RequestBody DynamicTaskEntity dynamicTask) {
    DynamicTaskEntity savedDynamicTask = dynamicTaskService.update(dynamicTask);
    return ResponseModelUtils.success(savedDynamicTask);
  }

  /**
   * 根据ID查询动态定时任务基本信息
   * @param id
   * @return
   */
  @GetMapping("{id}")
  @ApiOperation("根据ID查询动态定时任务基本信息")
  public ResponseModel findById(@PathVariable String id) {
    DynamicTaskEntity dynamicTask = dynamicTaskService.findById(id);
    return ResponseModelUtils.success(dynamicTask);
  }

  /**
   * 多条件分页查询
   * @param pageable
   * @param query
   * @return
   */
  @GetMapping("findByConditions")
  @ApiOperation(value = "多条件分页查询", notes = "分页条件为page和size,page从1开始,size默认50")
  public ResponseModel findByConditions(DynamicTaskQueryDTO query, @PageableDefault(page = 1, size = 50) Pageable pageable) {
    PageInfo page = dynamicTaskService.findByConditions(query, pageable);
    return ResponseModelUtils.success(page);
  }

  /**
   * 立即执行定时任务
   * @param code
   * @return
   */
  @GetMapping("execute")
  @ApiOperation("立即执行定时任务")
  public ResponseModel execute(@RequestParam @ApiParam("动态任务编码") String code) {
    dynamicTaskService.execute(code);
    return ResponseModelUtils.success();
  }

}
