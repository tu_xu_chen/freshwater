package org.wdsystem.rbac.utils;

import org.wdsystem.common.entity.OpUuidEntity;
import org.wdsystem.rbac.entity.UserEntity;

import java.time.LocalDateTime;

/**
 * 实体工具类
 * @author tuxuchen
 * @date 2022/7/22 15:35
 */
public class EntityUtils {

  private EntityUtils() {
    throw new UnsupportedOperationException();
  }

  /**
   * 初始化创建人信息
   * @param entity
   */
  public static void initCreatorInfo(OpUuidEntity entity) {
    UserEntity loginUser = SecurityUtils.getCurrentUser();
    LocalDateTime now = LocalDateTime.now();
    entity.setCreator(loginUser.getUserAccount());
    entity.setCreatorName(loginUser.getRealName());
    entity.setCreateTime(now);
    entity.setModifier(loginUser.getUserAccount());
    entity.setModifierName(loginUser.getRealName());
    entity.setModifyTime(now);
  }

  /**
   * 初始化修改人信息
   * @param entity
   */
  public static void initModifierInfo(OpUuidEntity entity) {
    UserEntity loginUser = SecurityUtils.getCurrentUser();
    LocalDateTime now = LocalDateTime.now();
    entity.setModifier(loginUser.getUserAccount());
    entity.setModifierName(loginUser.getRealName());
    entity.setModifyTime(now);
  }

}
