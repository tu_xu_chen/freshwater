package org.wdsystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动类
 * @author tuxuchen
 * @date 2022/7/22 14:17
 */
@SpringBootApplication
@MapperScan("org.wdsystem.**.mapper")
public class FreshwaterWebApplication {

  public static void main(String[] args) {
    SpringApplication.run(FreshwaterWebApplication.class, args);
  }

}
